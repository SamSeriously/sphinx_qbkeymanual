.. _usage-filevault:


==========
File Vault
==========

Create a vault
==============

After connecting to your QBKEY, the vault selection screen will be presented. Click the “Create or open a vault” button to continue. Windows explorer will be opened. Select or add a directory on a location of choice (harddisk, usb-disk, cloud drive, …)

.. important:: We recommend to start with an empty folder (if you take a non empty folder, the data in the directory won’t get added and will be ignored).

*Alternatively you can drag and drop your folder from the explorer window.*

.. image:: ../images/createvault.png



Open a vault
============

After connecting to your QBKEY, the vault selection screen will be presented. If you already have a vault you can choose your vault or connect the the last used vault (if enabled in the settings). When you mark a vault as favorite you will be have the option to open your favorite vault here as well.

.. image:: ../images/openvault.png

.. note:: you will only be able to see these first two options after you have enabled them in the settings screen. For more information on this topic see image below.

.. image:: ../images/lastvaultsettings.png



Adding folders
==============

Method 1 - New Folder Button
----------------------------

Click the “New Folder” button in the menu bar.

.. image:: ../images/newfolder.png

Enter a name for the folder. This will an empty folder.
You will be asked if you want to move or copy the folder and containing files.

.. image:: ../images/movecopy.png



Method 2 - Drag&Drop
--------------------

Folders can be added through drag&drop. Drag a folder from the explorer window or windows explorer onto the main QBKEY software window.
All files and subfolders of this directory will be added to the folder tree as well.
You will be asked if you want to move or copy the folder and containing files.


.. image:: ../images/movecopy.png


.. warning:: Empty folders will be deleted on exiting the software (this is a security measure).



Adding files
============

Method 1 - New File Button
----------------------------

Click the “New File” button in the menu bar.

.. image:: ../images/newfile.png

Browse the explorer window for the file you want to add and click the “open” button.
Remember, if you drag a folder, all files in the folder will be added.
You will be asked if you want to move or copy the files.

.. image:: ../images/movecopy.png

.. tip:: You can add multiple files at once by using the standard windows controls for selecting multiple file (holding shift or ctrl)



Method 2 - Drag&Drop
--------------------

Files can be added through drag&drop. Drag a file from the explorer windows onto the main QBKEY software window.
Remember, if you drag a folder, all files in the folder will be added.
You will be asked if you want to move or copy the files.

.. image:: ../images/movecopy.png




Deleting files and folders
==========================

You can select one or more files and folders.

.. tip:: You can select multiple files and folders at once by using the standard windows controls for selecting multiple file (holding shift or ctrl).

Right-click on a selected file and choose delete from the contextmenu.

**OR**

Click the “Delete files…” button in the file actions menu on the left side.
The standard delete function (shred) is a 5-pass operation.

.. note:: Deleting large files can take a very long time (because of the shredding procedure).

.. hint:: On deleting large files you will have to option to use a faster delete option, which is less secure than the standard delete option. The fast delete function (wipe) is a single-pass operation

Other file actions
==================
Open file
---------

Using this function will open certain file types in de QBKEY software. Supported file types are textfiles and images (more will be supported later).

Open in Windows
---------------

This option will open the selected file in assiociated windows software.

.. note:: opened files will be shown in the monitored files list (for more information see :ref:`usage-monitored`).

Rename
------
You can rename your files by using the rename button in the file action menu or by right clicking on a file and choosing “Rename” from the context menu.
