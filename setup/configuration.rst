.. _setup-configuration:

Configuration
#############

As soon as you connect to your QBKEY you can access the settings page by clicking the button in the top left.

.. image:: ../images/settingsbutton.png

General settings
================

The general settings let you change theme colors and language. Changes are applied immediatky upon selection.
If you do not need in-program tooltips with additional information, you can turn this option off by unticking the box. 


Security settings
=================

Enable password protection for all vaults
-----------------------------------------

This will ask for an additional password per vault. Use this option with care as there is no reset password option in case you forgot your password! Default setting = disabled.

Remember the last session
-------------------------

Enabling this will give you the option to select your last session in the vault selection screen.

Remember the last opened vault
------------------------------

This will give you  an extra option in the vault selection screen that takes you to your last opened vault. Default setting = disabled.

Automatically encrypt exported files with EFS
---------------------------------------------

Enables EFS encryption on exported files, which would otherwise be shared unencrypted. This can be overriden during the export process. Default setting = enabled.

Automatically clear the clipboard
---------------------------------

When this option is enabled, the clipboard will be cleared some time after you copied a password from a vault to your clipboard. This is a recommended security function that is enabled by default.

Automatically close the vault when the QBKEY is disconnected
------------------------------------------------------------

When you want to automatically close the vault when your USB key is disconnected, enable this option. 

Default generated password length
---------------------------------

This defines the length of the password generated in the password manager. When recommend to use very long password, as you don't have to remember them and increases entropy.


Device settings
===============

Device information
------------------

You can open the device information page of the connected key. This can also be done by clicking on the QBKEY icon in the lower left corner. Amongst other information, your fingerprint registration status is displayed. Also your public key is shown on this page.

Favorite Device
---------------

You can mark one of your QBKEYs as a favorite device. You can do this by clicking the device followed by clicking the “Favorite” button in the menu. Your favorite device will be marked by a star and will always be visible at the top of the list.



Vault settings
==============

Favorite vault
--------------

You can mark one of your vaults as a favorite vault. You can do this by clicking outlined star icon in the statusbar once you have a vault open. Once this you have a favorite vault, you can clear this setting by clicking the 'Forget favorite vault' button.


Extensions
==========

InterPlanetary FileSystem Support
---------------------------------

By enabling IPFS support you can share files directly with your contacts from the QBKEY software.

.. note:: Both you and your contact need to be connected to the ipfs network in order to operate. This can be checked in the right part of the statusbar. Connecting to the network does generate some network traffic in the background. Only data explicitly selected by you will be shared via the network. 


SSH Agent Support
-----------------

QBKEY can act as a SSH agent by enabling this option. A new tab 'SSH' will be shown on the main screen that allows you to manage your ssh-keys. 

.. note:: A restart of the software is needed after enabling this option.


Full Drive Encryption Management Support
----------------------------------------

.. warning:: Windows software version only! 

Enabling this module will allow you to manage your Bitlocker encrypted devices with QBKEY. A 'Drives' button will be enabled in the top menubar, where you can manage your Bitlocker drives. 

.. note:: A restart of the software is needed after enabling this option.
