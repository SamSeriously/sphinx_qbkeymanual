.. _setup-installation:


Installation
############

Installing the software
=======================

.. important:: Please make sure your operating system is completely up to date (check Windows Update)

Follow the link on the Downloads_ page. Press install (or press launch if you have all components pre-installed)

The install file will be downloaded. Once finished you can start-up the downloaded file.

Once the installation is finished, a shortcut will be placed on your desktop and the QBKEY software will start.


Setting up the hardware
=======================
Connect one of your QBKEYs with the provided USB-C cable to your computer. After a few seconds the key should show up in the list.

.. image:: ../images/deviceselection.jpg

If your QBKEY does not appear, press the 'Refresh' button on the top menu. When the key still does not show up in the list, try another USB-port on the computer (in case you don't have any spare ports, swap it with another connected device).



.. _Downloads: https://www.qbkey.com/qbkey-downloads/