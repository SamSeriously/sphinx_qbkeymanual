.. QBKEY Manual documentation master file, created by
   sphinx-quickstart on Sat May 12 08:10:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to QBKEY Manual's documentation!
========================================
.. image:: images/tri_keys.png

If this is the first time you use the QBKEY we suggest following all the steps as described in the ':ref:`setup-installation` section'.

Returning users can choose a topic in the ':ref:`setup-configuration` section' to get more information about a certain part of the software.


.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Setup Manual

   setup/installation
   setup/configuration
 
.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Usage Manual

   usage/filevault
   usage/contact
   usage/password
   usage/importexport
   usage/sharing
   usage/monitored

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Tips&Tricks 
   
   cloudstorage

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Troubleshooting 
   
   troubleshooting

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: FAQ

   faq/general
   faq/installation
   faq/usage
